package ru.yakgsras.earthquaketable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EarthquaketableApplication {

    public static void main(String[] args) {
        SpringApplication.run(EarthquaketableApplication.class, args);
    }
}
