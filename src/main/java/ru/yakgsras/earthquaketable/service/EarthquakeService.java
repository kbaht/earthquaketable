package ru.yakgsras.earthquaketable.service;

import org.springframework.data.domain.Page;
import ru.yakgsras.earthquaketable.domain.Earthquake;

public interface EarthquakeService {

    Earthquake create(Earthquake earthquake);

    Page<Earthquake> getEarthquakePage(int pageNumber, int size);

    Earthquake findEarthquakeById(Long id);

    Earthquake edit(Earthquake earthquake);
}
