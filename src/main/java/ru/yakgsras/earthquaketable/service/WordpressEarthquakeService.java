package ru.yakgsras.earthquaketable.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.yakgsras.earthquaketable.domain.Earthquake;
import ru.yakgsras.earthquaketable.util.EarthquakePageImpl;

import javax.annotation.PostConstruct;

@Service
public class WordpressEarthquakeService implements EarthquakeService {

    private final RestTemplate restTemplate;

    @Value("${wordpress.resturi}")
    public String requestUri;

    @Value("${wordpress.host}")
    public String host;

    private UriComponentsBuilder uriBuilder;

    public WordpressEarthquakeService(RestTemplateBuilder builder,
                                      @Value("${wordpress.token}") String token) {
        restTemplate = builder.interceptors((request, body, execution) -> {
            request.getHeaders().setContentType(MediaType.APPLICATION_JSON_UTF8);
            request.getHeaders().add("Authorization", "Basic " + token);
            return execution.execute(request, body);
        })
                .build();
    }

    @PostConstruct
    public void setUp() {
        uriBuilder = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host(host)
                .path(requestUri);
    }

    @Override
    public Earthquake create(Earthquake earthquake) {
        String uri = uriBuilder.build().toUriString();
        ResponseEntity<Earthquake> response = restTemplate.postForEntity(uri, earthquake, Earthquake.class);
        return response.getBody();
    }

    @Override
    public Earthquake edit(Earthquake earthquake) {
        String url = uriBuilder
                .cloneBuilder()
                .pathSegment(earthquake.getId().toString())
                .build()
                .toUriString();
        ResponseEntity<Earthquake> response = restTemplate.postForEntity(url, earthquake, Earthquake.class);
        return response.getBody();
    }

    @Override
    public Page<Earthquake> getEarthquakePage(int pageNumber, int size) {
        String url = uriBuilder
                .cloneBuilder()
                .queryParam("pageNumber", pageNumber)
                .queryParam("size", size)
                .build()
                .toUriString();
        return restTemplate.exchange(url, HttpMethod.GET,
                null, EarthquakePageImpl.class)
                .getBody();
    }

    @Override
    public Earthquake findEarthquakeById(Long id) {
        String url = uriBuilder
                .cloneBuilder()
                .pathSegment(id.toString())
                .build()
                .toUriString();
        ResponseEntity<Earthquake> entity = restTemplate.getForEntity(url, Earthquake.class);
        return entity.getBody();
    }
}
