package ru.yakgsras.earthquaketable.util;

import lombok.Data;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import ru.yakgsras.earthquaketable.domain.Earthquake;

import java.util.ArrayList;
import java.util.List;

@Data
public class EarthquakePageImpl extends PageImpl<Earthquake> {

    private long totalElements;
    private int totalPages;
    private int size;
    private int number;
    private List<Earthquake> content;

    public EarthquakePageImpl() {
        super(new ArrayList<>());
    }

    public EarthquakePageImpl(List<Earthquake> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public EarthquakePageImpl(List<Earthquake> content) {
        super(content);
    }


}
