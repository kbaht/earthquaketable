package ru.yakgsras.earthquaketable.web;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.yakgsras.earthquaketable.domain.Earthquake;
import ru.yakgsras.earthquaketable.service.EarthquakeService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.stream.IntStream;

@Controller
@AllArgsConstructor
public class EarthquakeController {

    private final EarthquakeService apiService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String list(Model model,
                       @RequestParam("page") Optional<Integer> pageNumber,
                       @RequestParam("size") Optional<Integer> size) {
        int currentPage = pageNumber.orElse(1);
        int pageSize = size.orElse(10);

        Page<Earthquake> page = apiService.getEarthquakePage(currentPage, pageSize);
        model.addAttribute("page", page);

        int totalPages = page.getTotalPages();
        if (totalPages > 0) {
            int[] pageNumbers = IntStream.rangeClosed(1, totalPages).toArray();
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "index";
    }

    @RequestMapping("/save")
    public String save(Model model) {
        boolean contains = model.containsAttribute("earthquake");
        System.out.println(contains);
        String placeholder = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        model.addAttribute("now", placeholder);
        Earthquake earthquake = new Earthquake();
        model.addAttribute("earthquake", earthquake);
        return "save";
    }

    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        Earthquake earthquake = apiService.findEarthquakeById(id);
        model.addAttribute("earthquake", earthquake);
        return "save";
    }


    @PostMapping("/request")
    public String submitForm(@ModelAttribute Earthquake earthquake, Model model) {
        System.out.println(earthquake.getId());
        if (earthquake.getId() == null) apiService.create(earthquake);
        else apiService.edit(earthquake);
        return "redirect:/?success=true";
    }
}
