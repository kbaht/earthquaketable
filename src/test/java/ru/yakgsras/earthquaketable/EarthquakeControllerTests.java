package ru.yakgsras.earthquaketable;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakgsras.earthquaketable.service.EarthquakeService;
import ru.yakgsras.earthquaketable.web.EarthquakeController;

@Ignore
@RunWith(SpringRunner.class)
@WebMvcTest(EarthquakeController.class)
public class EarthquakeControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EarthquakeService apiService;

//    @Test
//    public void shouldReturnIndexAndTwoModelAttributes() throws Exception {
//        this.mockMvc.perform(get("/").accept(MediaType.TEXT_HTML))
//                .andExpect(status().isOk())
//                .andExpect(view().name("index"))
//                .andExpect(model().attributeExists("now", "earthquake"))
//                .andExpect(model().attribute("now", isA(String.class)))
//                .andExpect(model().attribute("earthquake", isA(Earthquake.class)));
//    }
//
//    @Test
//    public void should() throws Exception {
//        Earthquake earthquake = new Earthquake(
//                null,
//                LocalDateTime.now().withNano(0),
//                BigDecimal.valueOf(56.65),
//                BigDecimal.valueOf(130.04),
//                10,
//                BigDecimal.valueOf(3.5),
//                BigDecimal.valueOf(10),
//                "somewhere"
//        );
//        String dateTimeString = earthquake.getDatetime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
//        ResponseEntity<Earthquake> responseEntity = new ResponseEntity<>(earthquake, HttpStatus.OK);
//
//        given(apiService.create(earthquake))
//                .willReturn(responseEntity);
//
//        this.mockMvc.perform(post("/request")
//                .accept(MediaType.APPLICATION_FORM_URLENCODED)
//                .param("datetime", dateTimeString)
//                .param("latitude", earthquake.getLatitude().toString())
//                .param("longitude", earthquake.getLongitude().toString())
//                .param("depth", String.valueOf(earthquake.getDepth()))
//                .param("magnitude", earthquake.getMagnitude().toString())
//                .param("energyclass", earthquake.getEnergyclass().toString())
//                .param("region", earthquake.getRegion()))
//                .andExpect(status().isOk())
//                .andExpect(model().attribute("code", responseEntity.getStatusCodeValue()))
//                .andExpect(model().attribute("status", "Успех"))
//                .andExpect(view().name("result"));
//    }
}
