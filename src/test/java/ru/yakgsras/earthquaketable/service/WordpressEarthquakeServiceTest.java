package ru.yakgsras.earthquaketable.service;


import org.junit.Test;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class WordpressEarthquakeServiceTest {

    @Test
    public void testUrlBuilder() {
        UriComponents build = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("localhost:8000")
                .path("?rest_route=/yakgsras/v1/earthquakes")
                .queryParam("pageNumber", 1)
                .queryParam("size", 10)
                .build();
        System.out.println(build.toUriString());
    }

}
